# .bashrc

## Based on ubuntu 18.04

## If not running interactively, don't do anything
case $- in
  *i*) ;;
    *) return;;
esac

export VISUAL=vim
export EDITOR="$VISUAL"

## Source global definitions
if [ -f /etc/bashrc ]; then
  . /etc/bashrc
fi

## Source local environment variables
if [ -f ${HOME}/.env ]; then
  . ${HOME}/.env
fi

## Source local aliases
if [ -f ${HOME}/.bash_aliases ]; then
  . ${HOME}/.bash_aliases
fi

## Locale settings
## Run "locale" to check values
# TODO: Add swedish layout?
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
export LC_COLLATE=C.UTF-8
export LC_CTYPE=en_US.UTF-8

## User specific aliases and functions

## don't put duplicate lines or lines starting with space in the history.
## See bash(1) for more options
HISTCONTROL=ignoreboth

## check the window size after each command and, if necessary,
## update the values of LINES and COLUMNS.
shopt -s checkwinsize

## If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

## Colors
## https://geoff.greer.fm/lscolors/
export CLICOLOR=1
alias LSCOLORS='di=0:ln=35:so=32:pi=33:ex=31:bd=34;46:cd=34;43:su=30;41:sg=30;41:tw=30;42:ow=30;43'
export LSCOLORS

## enable programmable completion features (you don't need to enable
## this, if it's already enabled in /etc/bash.bashrc and /etc/profile
## sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Set gitdir root folder

# This would be default
# Possibly create symlink if not present instead?
if [ -d ${HOME}/git ]; then
  gitfolder="${HOME}/git"

# WSL Windows c:\git
elif [ -d /mnt/c/git ]; then
  gitfolder="/mnt/c/git"

# WSL Windows d:\git
elif [ -d /mnt/d/git ]; then
  gitfolder="/mnt/d/git"

else
  echo "Unable to locate git dir..."
fi

# https://starship.rs
if [[ -f /usr/local/bin/starship ]]; then
    eval "$(starship init bash)"
else
    echo "Starship not found"
fi
