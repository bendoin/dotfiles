#!/usr/bin/env bash

# Syncs this folder to "${HOME}"

# TODO add flags, can't remember
set -euo pipefail

# rsync flags since I forget...
# v verbose
# r recursive
# u update skip files that are newer on receiver
# n dry run
# h human readable

## type can be used instead of hash
if hash rsync 2>/dev/null; then
  if [[ $(pwd | rev | cut -d '/' -f 1 | rev) == "dotfiles" ]]; then
    # rsync -vrunh --exclude '.git/' --exclude '*.md' . "${HOME}"
    rsync -vruh --exclude '.git/' --exclude '*.md' . "${HOME}"

    # shellcheck source=/dev/null
    source "${HOME}/.aliases"
    # shellcheck source=/dev/null
    source "${HOME}/.functions"
   else
    echo "Not in dotfiles directory, exiting."
   fi

  else
    echo "rsync not found, exiting..."
fi
