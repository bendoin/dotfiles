# Local fonts

Copy fonts to `~/.local/share/fonts/NAME/`

`~/.fonts` can be used too, but don't clutter the home :)

Run `fc-cache -fv`
