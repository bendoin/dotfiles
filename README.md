# dotfiles repo

## Installation

**Not working, just notes for now.**

Cloning the dotfiles:

```shell
if [ -d ~/git/dotfiles ]; then
        alias homegit="git --work-tree=$HOME --git-dir=$HOME/git/dotfiles"

elif [ -d /mnt/c/git/dotfiles ]; then
    alias homegit="git --work-tree=$HOME --git-dir=/mnt/c/git/dotfiles"

elif [ -d /mnt/d/git/dotfiles ]; then
        alias homegit="git --work-tree=$HOME --git-dir=/mnt/d/git/dotfiles"
fi

homegit init
homegit remote add origin https://gitlab.com/bendoin/dotfiles
homegit fetch
homegit checkout master
```

Based on ["Organising dotfiles in a git repository"](https://fuller.li/posts/organising-dotfiles-in-a-git-repository/)
