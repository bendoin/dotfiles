# .bash_aliases
# Unless bash specific, use .aliases instead

if [ -f ~/.aliases ]; then
  . ~/.aliases
fi
