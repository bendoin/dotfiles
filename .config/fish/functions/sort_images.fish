function sort_images --description "Sort JPG/ARW/MP4 in separate directories"
  mkdir jpg
  mkdir arw
  mkdir mp4

  find . -type f -iname "*.jpg" -exec mv {} jpg/ \;
  find . -type f -iname "*.arw" -exec mv {} arw/ \;
  find . -type f -iname "*.mp4" -exec mv {} mp4/ \;
end
