if type -q /usr/bin/exa
  alias ll "exa -alF"
else
  alias ll "ls -alF"
end
