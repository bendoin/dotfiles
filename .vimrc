" .vimrc
" `:source %` to reload saved file

" Start "sane" defaults
syntax on " Most languages

set noerrorbells " Disable sound
set tabstop=2 softtabstop=2
set shiftwidth=2
set expandtab " Tab to space
set smartindent
set nu "Line numbers
set nowrap
set smartcase " Case sensitive search until Uppercase
set noswapfile " Disables .swp files
set nobackup
set undodir=~./.vim/undodir " Must exist
set undofile
set incsearch " Results while you search
set colorcolumn=80
highlight ColorColumn ctermbg=0 guibg=lightgrey

" Less sane defaults (aka testing out)
let mapleader=" " " Set space to be leader key
" Plugins

" Testing out vim-plug
" `:PlugInstall` to install plugins
" Make sure you use single quotes

" https://github.com/junegunn/vim-plug/wiki/tips#automatic-installation
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

Plug 'morhetz/gruvbox' " Gruvbox theme
Plug 'mbbill/undotree'

call plug#end()

" Theme
colorscheme gruvbox
set background=dark

